Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      get 'statuses', to: 'statuses#index'
      post 'statuses', to: 'statuses#create'

      get 'categories', to: 'categories#index'
      post 'categories', to: 'categories#create'

      resources :todos
    end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
