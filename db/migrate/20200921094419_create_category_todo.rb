class CreateCategoryTodo < ActiveRecord::Migration[5.2]
  def change
    create_table :category_todos do |t|
      t.belongs_to :category
      t.belongs_to :todo
    end
  end
end
