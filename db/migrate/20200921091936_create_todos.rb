class CreateTodos < ActiveRecord::Migration[5.2]
  def change
    create_table :todos do |t|
      t.belongs_to :status
      t.timestamp :schedule_at
      t.string :title
      t.text :description

      t.timestamps
    end
  end
end
