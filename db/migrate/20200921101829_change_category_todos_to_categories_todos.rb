class ChangeCategoryTodosToCategoriesTodos < ActiveRecord::Migration[5.2]
  def change
    rename_table :category_todos, :categories_todos
  end
end
