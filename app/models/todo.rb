class Todo < ApplicationRecord
  belongs_to :status
  has_and_belongs_to_many :categories

  validates :status_id, presence: true
  validates :title, presence: true
  validates :description, presence: true
  validates :schedule_at, presence: true
end
