module Api
  module V1    
    class CategoriesController < ApplicationController
      def index
        categories = Category.all

        render json: ::Api::V1::CategorySerializer.new(categories).serialized_json
      end

      def create
        category = Category.new(category_params)

        if category.save
          render json: ::Api::V1::CategorySerializer.new(category).serialized_json, status: :created
        else
          render json: category.errors, status: :unprocessable_entity
        end
      end

      private

      def category_params
        params.require(:category).permit(:name)
      end
    end
  end
end
