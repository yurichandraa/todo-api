module Api
  module V1
    class TodosController < ApplicationController
      before_action :set_todo, only: %w[update destroy]

      def index
        todos = Todo.includes(:categories, :status)

        render json: ::Api::V1::TodoSerializer.new(todos, { include: [:categories, :status]}).serialized_json
      end

      def create
        todo = Todo.new(todo_params)
        todo.categories = Category.find(todo_params[:category_ids])

        if todo.save
          render json: ::Api::V1::TodoSerializer.new(todo).serialized_json, status: :created
        else
          render json: todo.errors, status: :unprocessable_entity
        end
      end

      def update
        todo = @todo.update(todo_params)
        @todo.categories = Category.find(todo_params[:category_ids])

        if @todo.save
          render json: ::Api::V1::TodoSerializer.new(@todo).serialized_json
        else
          render json: todo.errors, status: :unprocessable_entity
        end
      end

      def destroy
        if @todo.delete
          render status: :no_content
        else
          render json: todo.errors, status: :internal_server_error
        end
      end

      private

      def set_todo
        @todo = Todo.find(params[:id])
      end

      def todo_params
        params.require(:todo).permit(:status_id, :schedule_at, :title, :description, :category_ids => [])
      end
    end
  end
end
