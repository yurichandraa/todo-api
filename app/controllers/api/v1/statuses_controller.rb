module Api
  module V1
    class StatusesController < ApplicationController
      # GET /statuses
      def index
        statuses = Status.all

        render json: ::Api::V1::StatusSerializer.new(statuses).serialized_json
      end
    
      # POST /statuses
      def create
        status = Status.new(status_params)
    
        if status.save
          render json: ::Api::V1::StatusSerializer.new(status).serialized_json, status: :created
        else
          render json: status.errors, status: :unprocessable_entity
        end
      end
    
      private
        # Only allow a trusted parameter "white list" through.
        def status_params
          params.require(:status).permit(:name)
        end
    end 
  end
end
