module Api
  module V1
    class StatusSerializer
      include FastJsonapi::ObjectSerializer
      attributes :name
    end
  end
end
