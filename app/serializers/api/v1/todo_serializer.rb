module Api
  module V1
    class TodoSerializer
      include FastJsonapi::ObjectSerializer

      attributes :schedule_at, :title, :description
      
      belongs_to :status, serializer: ::Api::V1::StatusSerializer
      has_many :categories, serializer: ::Api::V1::CategorySerializer
    end
  end
end
