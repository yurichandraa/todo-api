module Api
  module V1    
    class CategorySerializer
      include FastJsonapi::ObjectSerializer

      attributes :name
    end
  end
end
