namespace :status do
  desc "Seed status data"
  task seed: :environment do
    data = [
      'Pending',
      'Ongoing',
      'Completed'
    ]

    data.each do |item|
      Status.find_or_create_by!(name: item)
    end
  end
end
