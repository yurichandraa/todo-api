namespace :category do
  desc "Seed category data"

  task seed: :environment do
    data = [
      'Backend',
      'Ruby',
      'Go',
      'Frontend',
      'VueJs'
    ]

    data.each do |item|
      Category.find_or_create_by!(name: item)
    end
  end
end
