require 'rails_helper'

RSpec.describe Api::V1::TodosController, :type => :request do
  describe '#index' do
    let!(:todo) { FactoryBot.create(:todo) }
    let(:status) { Status.first }
    let(:category) { Category.first }
    let(:expected_response) do 
      {
        'data' => [
          {
            'id' => todo.id.to_s,
            'type' => Todo.name.downcase,
            'attributes' => {
              'schedule_at' => todo.schedule_at.strftime('%Y-%m-%dT%H:%M:%S.%LZ'),
              'title' => todo.title,
              'description' => todo.description
            },
            'relationships' => {
              'status' => {
                'data' => {
                  'id' => status.id.to_s,
                  'type' => Status.name.downcase
                }
              },
              'categories' => {
                'data' => [
                  {
                    'id' => category.id.to_s,
                    'type' => Category.name.downcase
                  }
                ]
              }
            }
          }
        ],
        'included' => [
          {
            'id' => category.id.to_s,
            'type' => Category.name.downcase,
            'attributes' => {
              'name' => category.name
            }
          },
          {
            'id' => status.id.to_s,
            'type' => Status.name.downcase,
            'attributes' => {
              'name' => status.name
            }
          }
        ]
      }
    end

    it 'does return ok http status and its response' do
      get '/api/v1/todos'

      expect(response).to have_http_status :ok
      expect(JSON.parse(response.body)).to eq expected_response
    end
  end

  describe '#create' do
    let(:status) { FactoryBot.create(:status) }
    let(:category) { FactoryBot.create(:category) }

    context 'when param is valid' do
      let(:params) do
        {
          todo: {
            title: 'Fix bug',
            description: 'Fixing bug from create process',
            schedule_at: '2020-10-10',
            status_id: status.id,
            category_ids: [category.id]
          }
        }
      end

      let(:expected_response) do
        {
          'data' => {
            'id' => Todo.first.id.to_s,
            'type' => Todo.name.downcase,
            'attributes' => {
              'schedule_at' => Todo.first.schedule_at.strftime('%Y-%m-%dT%H:%M:%S.%LZ'),
              'title' => Todo.first.title,
              'description' => Todo.first.description
            },
            'relationships' => {
              'status' => {
                'data' => {
                  'id' => Status.first.id.to_s,
                  'type' => Status.name.downcase
                }
              },
              'categories' => {
                'data' => [
                  {
                    'id' => Category.first.id.to_s,
                    'type' => Category.name.downcase
                  }
                ]
              }
            }
          }
        }
      end

      it 'should return created status and its response' do
        post '/api/v1/todos', :params => params

        expect(response).to have_http_status :created
        expect(JSON.parse(response.body)).to eq expected_response
      end
    end

    context 'when param is empty or invalid' do
      it 'does return bad request status' do
        post '/api/v1/todos', :params => {}

        expect(response).to have_http_status :bad_request
      end
    end

    context 'when status_id payload is missing' do
      let(:params) do
        {
          todo: {
            title: 'Fix bug',
            description: 'Fixing bug from create process',
            schedule_at: '2020-10-10',
            category_ids: [category.id]
          }
        }
      end

      it 'does return unprocessable entity status' do
        post '/api/v1/todos', :params => params

        expect(response).to have_http_status :unprocessable_entity
      end
    end
  end

  describe '#update' do
    let!(:todo) { FactoryBot.create(:todo) }

    context 'when param is valid' do
      let(:params) do
        {
          id: todo.id,
          todo: {
            title: 'Fix bug',
            description: 'Fixing bug from create process',
            schedule_at: '2020-10-10',
            status_id: Status.first.id,
            category_ids: [Category.first.id]
          }
        }
      end

      let(:expected_response) do
        {
          'data' => {
            'id' => Todo.first.id.to_s,
            'type' => Todo.name.downcase,
            'attributes' => {
              'schedule_at' => Todo.first.schedule_at.strftime('%Y-%m-%dT%H:%M:%S.%LZ'),
              'title' => Todo.first.title,
              'description' => Todo.first.description
            },
            'relationships' => {
              'status' => {
                'data' => {
                  'id' => Status.first.id.to_s,
                  'type' => Status.name.downcase
                }
              },
              'categories' => {
                'data' => [
                  {
                    'id' => Category.first.id.to_s,
                    'type' => Category.name.downcase
                  }
                ]
              }
            }
          }
        }
      end

      it 'does render ok status and its response' do
        put "/api/v1/todos/#{todo.id}", :params => params

        expect(response).to have_http_status :ok
        expect(JSON.parse(response.body)).to eq expected_response
      end
    end

    context 'when resource is not found' do
      it 'does return not found status' do
        put "/api/v1/todos/#{2}"

        expect(response).to have_http_status :not_found
      end
    end
  end

  describe '#destroy' do
    let!(:todo) { FactoryBot.create(:todo) }

    context 'when resource is found' do
      it 'does return no content status' do
        delete "/api/v1/todos/#{todo.id}"

        expect(response).to have_http_status :no_content
      end
    end

    context 'when resource is not found' do
      it 'does return not found status' do
        delete "/api/v1/todos/#{2}"

        expect(response).to have_http_status :not_found
      end
    end
  end
end