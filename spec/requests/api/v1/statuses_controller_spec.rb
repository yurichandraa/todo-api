require "rails_helper"

RSpec.describe Api::V1::StatusesController, type: :request do
  describe '#index' do
    let!(:status) { FactoryBot.create(:status) } 
    let!(:expected_response) do
      {
        'data' => [
          {
            'id' => status.id.to_s,
            'type' => 'status',
            'attributes' => {
              'name' => status.name
            }
          }
        ]
      }   
    end

    it 'should return ok status and its response' do
      get '/api/v1/statuses'

      expect(response).to have_http_status(:ok)
      expect(JSON.parse(response.body)).to eql expected_response
    end
  end

  describe '#create' do
    context 'when param is valid' do
      it 'should return created status and return newly created status' do
        post '/api/v1/statuses', :params => { 'status' => { 'name' => 'test' }}

        expected_response = {
          'data' => {
            'id' => Status.first.id.to_s,
            'type' => 'status',
            'attributes' => {
              'name' => Status.first.name
            }
          }
        }

        expect(response).to have_http_status(:created)
        expect(JSON.parse(response.body)).to eq(expected_response)
      end
    end
  end
end
