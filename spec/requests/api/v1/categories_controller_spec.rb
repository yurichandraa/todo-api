require 'rails_helper'

RSpec.describe Api::V1::CategoriesController, type: :request do
  describe '#index' do
    let!(:category) { FactoryBot.create(:category) }
    let!(:expected_response) do
      {
        'data' => [
          {
            'id' => category.id.to_s,
            'type' => 'category',
            'attributes' => {
              'name' => category.name
            }
          }
        ]
      }
    end

    it 'should return ok status and its response' do
      get '/api/v1/categories'

      expect(response).to have_http_status(:ok)
      expect(JSON.parse(response.body)).to eql expected_response
    end
  end

  describe '#create' do
    let(:category) { Category.first }

    it 'should return created status and newly created category' do
      post '/api/v1/categories', :params => { 'category' => { 'name' => 'test' }}

      expect(response).to have_http_status(:created)
      expect(JSON.parse(response.body)).to eq({
        'data' => {
          'id' => category.id.to_s,
          'type' => 'category',
          'attributes' => {
            'name' => category.name
          }
        }
      })
    end
  end
end
