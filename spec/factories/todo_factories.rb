FactoryBot.define do
  factory :todo do
    id { '1' }
    schedule_at { "2020-09-21 16:19:36" }
    title { "MyString" }
    description { "MyText" }

    # This line for define status factory while create todo
    status factory: :status
    
    # This line for define category factory after create todo
    after(:create) do |todo, evaluator|
      create_list(:category, 1, todos: [todo])

      todo.reload
    end
  end
end
